<?php
require 'model/data.php';
$donnees = getCatProducts('3');
require_once 'vendor/autoload.php';
$loader = new Twig\Loader\FilesystemLoader('Views');
$options_prod = array('cache' => 'cache', 'autoescape' => true);
$options_dev = array('cache' => false, 'autoescape' => true);
$twig = new Twig\Environment($loader);
$parameters = array('title' => 'Fruits secs', 'products' => $donnees);
echo $twig->render('catProducts.twig',$parameters);