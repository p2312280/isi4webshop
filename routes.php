<?php
return [
    '/' => 'HomeController@index',
    '/boissons' => 'ProductController@boissons',
    '/biscuits' => 'ProductController@biscuits',
    '/fruits_secs' => 'ProductController@fruitsSecs',
    '/login' => 'LoginController@showLoginForm',
];
