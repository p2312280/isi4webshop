<?php

namespace App\Controllers;

use Twig\Environment;

class ProductController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function boissons()
    {
        return $this->twig->render('boissons.twig');
    }

    public function biscuits()
    {
        return $this->twig->render('biscuits.twig');
    }

    public function fruitsSecs()
    {
        return $this->twig->render('fruits_secs.twig');
    }
}
