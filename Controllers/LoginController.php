<?php
namespace App\Controllers;

use PDO;
use Twig\Environment;

class LoginController
{
    private $twig;
    private $pdo;

    public function __construct(Environment $twig, PDO $pdo)
    {
        $this->twig = $twig;
        $this->pdo = $pdo;
    }

    public function index()
    {
        return $this->twig->render('login.twig');
    }

    public function loginSubmit()
    {
        // Handle login form submission
        // Validate user credentials, check against the database, etc.
        // For simplicity, let's assume the login is successful for any username/password combination

        // You might want to use password_hash() for securely storing passwords in the database
        // and password_verify() for checking the entered password against the hashed one.

        // For example, if authentication is successful:
        $_SESSION['user'] = ['username' => $_POST['username']];

        // Redirect to a protected page or the user's dashboard
        header('Location: dashboard.php'); // Update the path to your dashboard or protected page
        exit;
    }
}
