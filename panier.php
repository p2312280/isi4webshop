<?php

require_once 'vendor/autoload.php';

use Twig\Loader\FilesystemLoader;
use Twig\Environment;

// Specify the path to your Twig templates
$loader = new FilesystemLoader('Views');

// Create a Twig environment
$twig = new Environment($loader);

// Render the template
echo $twig->render('panier.twig');