<?php
function getBdd() {
    $bdd = new PDO('mysql:host=localhost;dbname=isiweb4shop;charset=utf8', 'root', '');
    return $bdd;
}

function getAllProducts() {
    $bdd = getBdd();
    $products = $bdd->query('SELECT * FROM products');
    
    // Fetch data as an associative array
    $donnees = $products->fetchAll(PDO::FETCH_ASSOC);
    
    return $donnees;
}

function getCatProducts(int $cat_id) {
    $bdd = getBdd();
    $products = $bdd->query('SELECT * FROM products where cat_id='.$cat_id);
    
    // Fetch data as an associative array
    $donnees = $products->fetchAll(PDO::FETCH_ASSOC);
    
    return $donnees;
}



